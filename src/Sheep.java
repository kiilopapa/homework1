
public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
       Animal[] loomad = {Animal.sheep, Animal.goat} ;
       reorder(loomad);
      // for debugging
   }
   
   public static void reorder (Animal[] animals) {
       int l = 0;
       int r = animals.length -1;

       while (l < r){
           if (animals[l].compareTo(Animal.goat) == 0){
               l++;
           }else if (animals[r].compareTo(Animal.sheep) == 0){
               r--;
           }else {
               Animal tmp = animals[l];
               animals[l] = animals[r];
               animals[r] = tmp;
           }
       }

//       for (Animal animal : animals) {
//           System.out.println(animal.compareTo(Animal.sheep));
//           System.out.println(Animal.goat.compareTo(animal));

//       }

//       sort(animals, l, r);




//       for (Animal animal : animals) {
//           System.out.println(animal.compareTo(Animal.sheep));
//           System.out.println(Animal.goat.compareTo(animal));

//       }
   }

    private static void sort(Animal[] animals, int l, int r) {
        if (animals.length<2) return;
        int i = l;
        int j = r;
        do {
            while (animals[i].compareTo(Animal.sheep) > 0) i++;
            while (Animal.goat.compareTo(animals[j]) > 0) j--;
            if (i <= j){
                Animal tmp = animals[i];
                animals[i] = animals[j];
                animals[j] = tmp;
                i++;
                j--;
            }
        } while ( i < j);
        if (l < j) sort (animals, l, j+1);
        if (i < r-1) sort(animals, i, r);
    }
}

